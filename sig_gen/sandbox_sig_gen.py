#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 18:19:51 2023

@author: elio
"""

from statsmodels.tsa.arima_process import arma_generate_sample
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import sosfiltfilt, butter

# plt.close('all')

#%%

def lognormal_transient(x, mu, sigma):
    
    y = (1/(x*sigma*np.sqrt(2*np.pi)))*np.exp(-((np.log(x)-mu)**2)/(2*sigma**2))
    
    # y_median = np.exp(mu)
    # y_mean = np.exp(mu+.5*sigma**2)
    # y_mode = np.exp(mu-sigma**2)
    
    # return a normalized for maximum version of y
    return y/y.max()


def arma_ts(sig_len):
    
    arparams = np.array([.75, -.25, .1])
    maparams = np.array([1, .3, -.01])
    ar = np.r_[1, -arparams] # add zero-lag and negate
    ma = np.r_[1, maparams] # add zero-lag
    y = arma_generate_sample(ar, ma, sig_len)
    
    return y/y.std()


def simulate_dataset(nsubjs=30, nsecs=2, fsample=256, SNR=.1, lpfilter=30):
    
    xtime = np.arange(1/fsample, nsecs+1/fsample, step=1/fsample)
    sim_sample = np.empty((nsecs*fsample, nsubjs))
        
    for isubj in range(nsubjs):
        
        mu = np.random.randn()*.001
        sigma = np.abs(np.random.randn()*.1 + .15) 
        this_SNR = np.random.randn()*.1 + SNR
        y_signal = lognormal_transient(xtime, mu, sigma)*this_SNR
        
        y_noise = arma_ts(fsample*nsecs)
        
        sim_sample[:,isubj] = y_signal+y_noise
        
    if lpfilter:
        
        my_LP_filter = butter(5, lpfilter, 'low', fs=fsample, output='sos')
        sim_sample = sosfiltfilt(my_LP_filter, sim_sample, axis=0)
        
    return sim_sample


#%% run

sim_sample = simulate_dataset(SNR=.7)
tvals_sample = np.sqrt(30)*sim_sample.mean(axis=1)/sim_sample.std(axis=1)

plt.figure()
plt.plot(tvals_sample)

plt.figure(); plt.subplot(211)
plt.plot(sim_sample[:, 0])

plt.subplot(212)
xfreqs = np.fft.rfftfreq(2*256, d=1/256)
plt.plot(xfreqs, np.abs(np.fft.rfft(sim_sample, axis=0)).mean(axis=1))
